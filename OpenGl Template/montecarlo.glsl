// shadertype=glsl

#version 330
#extension GL_EXT_gpu_shader4 : enable

uniform vec2 resolution;
uniform sampler2D env_map_texture;
uniform vec2 look_at_angles;
uniform int noise_seed = 13;

uniform int sphere_switch;
uniform int brdf_number;
uniform bool importance;

uniform int samples;
uniform float shininess;
uniform vec3 color_specular;
uniform vec3 color_diffuse;

vec3 eye = vec3(0.0, 0.0, 0.0);
vec3 forward;
vec3 right;
vec3 up;
vec3 lookat;

/* -----------------------------------------------------------
 * Random number generator
 * -----------------------------------------------------------
 */
unsigned int z1, z2, z3, z4;

void initRNG()
{
    z1 = 128U + uint(gl_FragCoord.x);
    z2 = 128U + uint(gl_FragCoord.y);
    z3 = 128U + uint(gl_FragCoord.x + gl_FragCoord.y);
    z4 = 128U + uint(gl_FragCoord.x * gl_FragCoord.y);
}

/*
 * S1, S2, S3, and M are all constants, and z is part of the
 * private per-thread generator state.
 */
unsigned int TausStep(inout unsigned int z, 
                      int S1,
                      int S2,
                      int S3,
                      unsigned int M) 
{
    unsigned int b = (((z << uint(S1)) ^ z) >> uint(S2));
    return z = (((z & M) << uint(S3)) ^ b);
}

// A and C are constants
unsigned int LCGStep(inout unsigned int z, unsigned int A, unsigned int C) 
{
    return z = (A * z + C);
}

float HybridTaus() 
{
    // Combined period is lcm(p1,p2,p3,p4)~ 2^121
    return 2.3283064365387e-10
            *float( // Periods
                    TausStep(z1, 13, 19, 12, 4294967294U) ^  // p1=2^31-1
                    TausStep(z2, 2, 25, 4, 4294967288U) ^    // p2=2^30-1
                    TausStep(z3, 3, 11, 17, 4294967280U) ^   // p3=2^28-1
                    LCGStep(z4, 1664525UL, 1013904223U)        // p4=2^32
                    );
}

#define NUM_SPHERES (10)
#define M_PI ( 2.0 * asin(1.0))

struct Ray
{
    vec3 origin;
    vec3 direction;
};

struct Sphere
{
    vec3 center;
    float radius;
    vec4 color;
};

struct Plane
{
    vec3 p0;
    vec3 r1;
    vec3 r2;
    vec4 color;
};

Plane plane;
Sphere spheres[NUM_SPHERES];

Ray makeRay(vec3 origin, vec3 direction)
{
    Ray ray;

    ray.origin = origin;
    ray.direction = normalize(direction);

    return ray;
}

Plane makePlane(vec3 p0, vec3 r1, vec3 r2, vec4 color)
{
    Plane plane;

    plane.p0 = p0;
    plane.r1 = normalize(r1);
    plane.r2 = normalize(r2);
    plane.color = color;

    return plane;
}

Sphere makeSphere(in vec3 center, in float radius, in vec4 color)
{
    Sphere sphere;

    sphere.center = center;
    sphere.radius = radius;
    sphere.color = color;

    return sphere;
}

bool intersectPlane(Ray ray, Plane plane, out float distance)
{
    vec3 normal = normalize(cross(plane.r1, plane.r2));

    float z = dot(ray.origin - plane.p0, normal);
    float n = dot(ray.direction, normal);

    distance = 0.0;

    if (abs(n) > 1.0e-6)
    {
        distance = -(z / n);
        return distance >= 0.0;
    }

    return false;
}

bool intersectSphere(in Ray ray, in Sphere sphere, out float t1, out float t2)
{
    vec3 p = ray.origin - sphere.center;

    float a = dot(ray.direction, ray.direction);
    float b = 2.0 * dot(p, ray.direction);
    float c = dot(p, p) - (sphere.radius * sphere.radius);

    float det = b * b - 4.0 * a * c;

    t1 = 0.0;
    t2 = 0.0;

    if (det < 0.0)
    {
        return false;
    }

    det = sqrt(det);

    float q;

    if (b < 0.0)
    {
        q = -0.5 * (b - det);
    }
    else
    {
        q = -0.5 * (b + det);
    }

    t1 = q / a;
    t2 = c / q;

    return true;
}

vec3 sphericalToCartesian(vec3 spherical) 
{

    float phi = spherical.x;
    float theta = spherical.y;
    float r = spherical.z;

    vec3 cart;

    cart.x = sin(theta) * cos(phi);
    cart.y = cos(theta);
    cart.z = sin(theta) * sin(phi);

    return cart;
}

vec3 cartesianToSpherical(vec3 cart) 
{

    float phi = atan(cart.x, cart.z);
    float theta = acos(cart.y);
    float r = length(cart);

    return vec3(phi, theta, r);
}

vec3 sampleEnvMap(vec3 dir)
{
    vec3 color = vec3( 0.0);

    vec3 angles;
    vec2 coords;

    angles = cartesianToSpherical( dir);

    color = texture2D( env_map_texture,
                       vec2( 1.0 - angles.x/( 2.0 * M_PI), angles.y / M_PI)).xyz;

    return color;
}

vec3 compProd(vec3 a, vec3 b) 
{
    return vec3(a.r * b.r, a.g * b.g, a.b * b.b);
}

/* -----------------------------------------------------------
 * BRDF functions
 * -----------------------------------------------------------
 */

// n = normal; v = ray.direction; l = light.direction
vec3 phongBRDF( vec3 n,
                vec3 v,
                vec3 l,
                vec3 diffuse,
                vec3 specular,
                float exponent)
{
    vec3 r = reflect(-v, n);
	
    // diffuse term
    vec3 diff = diffuse / M_PI;

    // specular term
    float spec = 10.0;
    spec *= pow(max(0.0, dot(r, l)), exponent);

    return diff + specular * spec;
}

vec3 blinnPhongBRDF( vec3 n,
                     vec3 v,
                     vec3 l,
                     vec3 diffuse,
                     vec3 specular,
                     float exponent)
{
    vec3 h = normalize(l + v);

    // diffuse term
    vec3 diff = diffuse / M_PI;

    // specular term
    float spec = (exponent + 2.0) / ( 2.0 * M_PI);
    spec *= pow( dot(h, n), exponent);

    return diff + specular * spec;
}


vec3 lambertBRDF(vec3 diffuse)
{
    return diffuse/M_PI;
}

/* -----------------------------------------------------------
 * Uniform Sampling
 * -----------------------------------------------------------
 */
vec3 getUniformRandomSampleDirectionUpper(in vec3 N)
{
	/*
	 * Uniform Sampling of the hemisphere
     * N: normal of the plane seperating the two hemispheres of the sphere
     * return value: uniformly randomly distributed sample direction into the hemisphere pointed at by N
	 */
    vec3 sample_direction = vec3(0.0);
    float z = 0.0, phi = 0.0;

    z = 2.0 * (HybridTaus() - 0.5);
    phi = 2.0 * M_PI * HybridTaus();
    sample_direction = vec3(sqrt(1-pow(z,2)) * cos(phi), z, sqrt(1-pow(z,2)) * sin(phi));

    if (dot(N, sample_direction) < 0.0)
        sample_direction *= -1.0;

    return sample_direction;
}

vec3 uniformSampleLambert(in vec3 N, in vec3 diffuse)
{
	/*
	 * Uniform Sampling, Lambert BRDF
     * N: normal at intersection point of sphere
     * diffuse: color of sphere
     * return value: color sample of incident light correctly weighted by several factors (angle, pdf, brdf...)
	 */
    vec3 color = color_diffuse;
    vec3 l_dir = vec3(0.0);

    l_dir = getUniformRandomSampleDirectionUpper(N);
    color = 2.0 * M_PI * dot(N, l_dir) * lambertBRDF(sampleEnvMap(l_dir)) * diffuse;

    return color;
}

vec3 uniformSamplePhong(in vec3 V, in vec3 N, in vec3 diffuse,
                        in vec3 specular, in float shiny)
{
	/*
	 * Uniform Sampling, Phong BRDF
     * N: normal at intersection point of sphere
     * V: view vector
     * diffuse: color of sphere
     * specular: specular property of the sphere
     * shiny: shininess of the sphere
     * return value: color sample of incident light correctly weighted by several factors (angle, pdf, brdf...)
	 */
    vec3 color = color_diffuse;
    vec3 l_dir = vec3(0.0);

    l_dir = getUniformRandomSampleDirectionUpper(N);
    color = 2.0 * M_PI * dot(N, l_dir) * phongBRDF(N, V, l_dir, sampleEnvMap(l_dir) * diffuse, 
        sampleEnvMap(l_dir) * specular, shiny);

	return color;
}

/* -----------------------------------------------------------
 * Importance sampling
 * -----------------------------------------------------------
 */
vec3 orthogonalVector(vec3 n) {

    if((abs(n.y) >= 0.9*abs(n.x)) && (abs(n.z) >= 0.9*abs(n.x)))
        return vec3(0.0, -n.z, n.y);
    else
        if((abs(n.x) >= 0.9*abs(n.y)) && (abs(n.z) >= 0.9*abs(n.y)))
            return vec3(-n.z, 0.0, n.x);
        else
            return vec3(-n.y, n.x, 0.0);
}

void localFrame(in vec3 n, out vec3 a, out vec3 b) {

    a = normalize(orthogonalVector(n));
    b = normalize(cross(a, n));
}

mat3 localTransformation(vec3 n) {

    vec3 a, b;
    localFrame(n, a, b);

    mat3 A = mat3(
                b.x, b.y, b.z, // first column (not row!)
                a.x, a.y, a.z, // second column
                n.x, n.y, n.z  // third column
                );

    return A;
}

vec3 cosineHemisphereSample(float u1, float u2, float e) 
{
	/*
	 * Vector constuction based on the cosinus distribution
     * u1, u2: distribution parameters
     * e: exponent
     * return value: sample direction
	 */
	vec3 dir = vec3(0.0);
    
    dir.x = sqrt(1.0-pow(u2, 2.0/(e+1.0))) * cos(2.0 * M_PI * u1);
    dir.y = sqrt(1.0-pow(u2, 2.0/(e+1.0))) * sin(2.0 * M_PI * u1);
    dir.z = pow(u2, 1.0/(e+1.0));

	return dir;
}

float cosineHemispherePDF(float cos_theta, float e) 
{
	/*
     * PDF calculation for cosinus distribution
     * cos_theta: cosine of theta, as shown in the figure in the worksheet
     * e: exponent
	 */
	float pdf = 0.0;
    
    pdf = (e+1.0) / (2.0*M_PI) * pow(cos_theta,e); 

	return pdf;
}

vec3 importanceSampleLambert(in vec3 N, in vec3 diffuse)
{
	/*
     * N: normal at intersection point of sphere
     * diffuse: color of sphere
     * Hemisphere with cos^e importance sampling, Lambert BRDF
     * use HybridTaus() for random numbers
	 */
	vec3 color = color_diffuse;
    vec3 l_dir = vec3(0.0);
    float cos_theta = 0.0, e = 1.0;

    l_dir = cosineHemisphereSample(HybridTaus(), HybridTaus(), e);
    cos_theta = dot(vec3(0.0, 0.0, 1.0), l_dir);
    l_dir = localTransformation(N) * l_dir;
    color = 1.0 / cosineHemispherePDF(cos_theta, e) * dot(N, l_dir) * lambertBRDF(sampleEnvMap(l_dir)) * diffuse;

	return color;
}

vec3 importanceSamplePhong(in vec3 V, in vec3 N, in vec3 diffuse,
                           in vec3 specular, in float shiny)
{
	/*
     * N: normal at intersection point of sphere
     * V: view vector
     * diffuse: color of sphere
     * specular: specular property of the sphere
     * shiny: shininess of the sphere
     * return value: color sample of incident light weighted by several factors (angle, pdf, brdf...)
     * Hemisphere with cos^e importance sampling, Phong BRDF
     */
	vec3 color = color_diffuse;
    vec3 l_dirDiff = vec3(0.0);
    vec3 l_dirSpec = vec3(0.0);
    float cos_theta = 0.0;

    l_dirDiff = getUniformRandomSampleDirectionUpper(N);
    l_dirSpec = cosineHemisphereSample(HybridTaus(), HybridTaus(), shiny);
    cos_theta = dot(vec3(0.0, 0.0, 1.0), l_dirSpec);
    l_dirSpec = localTransformation((2.0 * dot(N, V) * N) - V) * l_dirSpec;
    color = phongBRDF( N, V, l_dirDiff, sampleEnvMap(l_dirDiff) * diffuse * dot(N,l_dirDiff), 
                    vec3(0.0), shiny);
    color += phongBRDF( N, V, l_dirSpec, vec3(0.0), 
                    sampleEnvMap(l_dirSpec) * specular * dot(N,l_dirSpec), shiny);
    color *= 1.0 / cosineHemispherePDF(cos_theta, shiny);

	return color;
}

vec4 radiance(Ray ray, vec3 eye)
{
    vec4 color = vec4(0.0, 0.0, 0.0, 0.0);
    float minDistance = 1000000000.0;
    float planeDistance = 0.0;
    int hit_sphere = 0;

    // ray-sphere-intersection
    for (int i = 0; i < NUM_SPHERES; ++i) {
        float t1, t2;

        if (intersectSphere(ray, spheres[i], t1, t2)) {
            float distance = min(t1, t2);

            if (distance > 0.0 && distance < minDistance) {
                hit_sphere = 1;
                minDistance = distance;

                vec3 p = ray.origin + minDistance * ray.direction;
                vec3 n = normalize(p - spheres[i].center);
                vec3 l = normalize(vec3(1.0, 5.0, 0.0) - p);
                vec3 v = normalize(eye - p);

                for(int j = 0; j < samples; ++j) {
					
                    switch(brdf_number) {
                    
					case(0):
						if(!importance)
							color.rgb += uniformSampleLambert(n, color_diffuse);
						else
							color.rgb += importanceSampleLambert(n, color_diffuse);
                        break;

                    case(1):
						if(!importance)
							color.rgb += uniformSamplePhong(v, n, color_diffuse, color_specular, shininess);
						else
							color.rgb += importanceSamplePhong(v, n, color_diffuse, color_specular, shininess);
                        break;

                    default:
                        color += spheres[i].color;
                    }
                }

                color /= float(samples);
            }
        }
    }

    if(hit_sphere == 0)
	{
        color = vec4(sampleEnvMap(ray.direction), 1.0);
    }

    color.a = 1.0;
    return color;
}


vec4 gamma(vec4 color)
{
    // Gamma-Correction
    clamp(color.rgb, 0.0, 1.0);

    const float magic = 0.0031308;
    const float factor = 12.92;
    const float a = 0.055;
    const float aPlus1 = a + 1.0;
    const float inv24 = 1.0 / 2.4;

    color.r = (color.r <= magic) ? (color.r * factor) : (aPlus1 * pow(color.r, inv24) - a);
    color.g = (color.g <= magic) ? (color.g * factor) : (aPlus1 * pow(color.g, inv24) - a);
    color.b = (color.b <= magic) ? (color.b * factor) : (aPlus1 * pow(color.b, inv24) - a);
    color.a = 1.0;

    return color;
}


void initWorld()
{
    plane = makePlane(vec3(0.0, -2.0, -10.0), 
						vec3(1.0, 0.0, 0.0), 
						vec3(0.0, 0.0, 1.0), 
						vec4(1.0, 1.0, 0.0, 1.0));

    vec4 spcol[NUM_SPHERES];
    spcol[0] = vec4(1.0, 1.0, 1.0, 1.0);
    spcol[1] = vec4(1.0, 1.0, 0.0, 1.0);
    spcol[2] = vec4(1.0, 0.0, 1.0, 1.0);
    spcol[3] = vec4(0.0, 0.0, 1.0, 1.0);
    spcol[4] = vec4(0.0, 1.0, 0.5, 1.0);
    spcol[5] = vec4(0.5, 1.0, 0.3, 1.0);
    spcol[6] = vec4(1.0, 0.5, 0.2, 1.0);
    spcol[7] = vec4(0.3, 1.0, 0.4, 1.0);
    spcol[8] = vec4(0.4, 0.2, 0.8, 1.0);
    spcol[9] = vec4(0.0, 1.0, 0.0, 1.0);


    spheres[0] = makeSphere(vec3(0.0, 0.0, -1.0) * 2.0, 0.5, spcol[0]);
    spheres[1] = makeSphere(vec3(-1.0, 0.0, 0.0) * 2.0, 0.5, spcol[1]);
    spheres[2] = makeSphere(vec3(1.0, 0.0, 0.0) * 2.0, 0.5, spcol[2]);
    spheres[3] = makeSphere(vec3(0.0, 1.0, 0.0) * 2.0, 0.5, spcol[3]);
    spheres[4] = makeSphere(vec3(0.0, -1.0, 0.0) * 2.0, 0.5, spcol[4]);

    spheres[5] = makeSphere(vec3(0.0, 0.0, 1.0) * 2.0, 0.5, spcol[5]);
    spheres[6] = makeSphere(normalize(vec3(1.0, 0.5, -1.0)) * 2.0, 0.5, spcol[6]);
    spheres[7] = makeSphere(normalize(vec3(1.0, 1.0, 1.0)) * 2.0, 0.5, spcol[7]);
    spheres[8] = makeSphere(normalize(vec3(-1.0, -0.5, 1.0)) * 2.0, 0.5, spcol[8]);
    spheres[9] = makeSphere(normalize(vec3(-1.0, -1.0, -1.0)) * 2.0, 0.5, spcol[9]);

}


void initCamera()
{
    vec3 vert = vec3(0.0, 1.0, 0.0);

    lookat = sphericalToCartesian( vec3(look_at_angles, 1.0) );

    float fov = 45.0 / 180.0 * 3.1415962;
    float ratio = float(resolution.x) / float(resolution.y);

    // Default
    forward = vec3(0.0, 0.0, -1.0);
    right = vec3(1.0, 0.0, 0.0) * float(resolution.x) / float(resolution.y);
    up = vec3(0.0, 1.0, 0.0);


    float tan_fov = 2.0 * tan(fov * 0.5);

    forward = normalize(lookat);
    right = normalize(cross(forward, vert)) * tan_fov * ratio;
    up = normalize(cross(right, forward)) * tan_fov;


}


Ray getRayOrtho(float x, float y)
{
    return makeRay(eye + (((2.0 * x / resolution.x) - 1.0) * right)
                   + (((2.0 * y / resolution.y) - 1.0) * up)
                   , forward);
}


Ray getRayPerspective(float x, float y)
{

    return makeRay(eye
                   , forward + (((2.0 * x / resolution.x) - 1.0) * right)
                   + (((2.0 * y / resolution.y) - 1.0) * up));
}


vec4 getColor(float x, float y)
{
    Ray ray = getRayPerspective(x, y);
    vec4 color = radiance(ray, eye);

    return color;
}

void main(void)
{
    initWorld();
    initCamera();
    initRNG();

    vec4 color = getColor(gl_FragCoord.x + 0.5, gl_FragCoord.y + 0.5);
    color = gamma(color);

    gl_FragColor = color;
}
