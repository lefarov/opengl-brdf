#include "Engine.h"
#include <cstdlib>

/* ----------------------------------------------------------
 * GLFW Callbacks
 * ----------------------------------------------------------
 */

#define M_PI 3.141592f
#define EPSILON 0.000001f

void error_callback(int error, const char *description) 
{
	fprintf(stderr, "Error : %s\n", description);
}

static void key_callback(GLFWwindow *window, int key, int scancode, int action, int mods)
{
	if(key == GLFW_KEY_ESCAPE && action == GLFW_PRESS)
	{
		glfwSetWindowShouldClose(window, GLFW_TRUE);
	}

	Engine * engine = static_cast<Engine *>(glfwGetWindowUserPointer(window));
	float azimuth = engine->GetCameraAzimuth();
	float polar = engine->GetCameraPolar();

	switch (key) 
	{
	case GLFW_KEY_A:
		azimuth -= 3.0f * float(M_PI) / 180.0f;
		azimuth = azimuth < 0.0f ? 2.0f * float(M_PI) + azimuth : azimuth;
		break;

	case GLFW_KEY_D:
		azimuth += 3.0f * float(M_PI) / 180.0f;
		azimuth = azimuth > 2.0f * float(M_PI) ? azimuth - 2.0f * float(M_PI) : azimuth;
		break;

	case GLFW_KEY_W:
		polar -= 3.0f *float(M_PI) / 180.0f;
		polar = polar < 0.0f ? 0.0f + EPSILON : polar;
		break;

	case GLFW_KEY_S:
		polar += 3.0f *float(M_PI) / 180.0f;
		polar = polar > float(M_PI) ? float(M_PI) - EPSILON : polar;
		break;
	}

	engine->SetCameraAzimuth(azimuth);
	engine->SetCameraPolar(polar);

	TwEventKeyGLFW(key, action);
}

static void cursor_position_callback(GLFWwindow *window, double xpos, double ypos)
{
	TwEventMousePosGLFW(int(xpos), int(ypos));
}

void mouse_button_callback(GLFWwindow *window, int button, int action, int mods)
{
	TwEventMouseButtonGLFW(button, action);
}

void scroll_callback(GLFWwindow *window, double xoffset, double yoffset)
{
	TwEventMouseWheelGLFW(yoffset);
}

void character_callback(GLFWwindow *window, unsigned int codepoint)
{
	TwEventCharGLFW(codepoint, GLFW_PRESS);
}

void window_size_callback(GLFWwindow *window, int width, int height)
{
	TwWindowSize(width, height);
}

void framebuffer_size_callback(GLFWwindow *window, int width, int height)
{
	Engine * engine = static_cast<Engine *>(glfwGetWindowUserPointer(window));

	engine->SetFBWidth(width);
	engine->SetFBHeight(height);

	gl::Viewport(0, 0, width, height);
	TwWindowSize(width, height);
}

void close_callback(GLFWwindow *window)
{
	printf("Window closed");
}

void freeimage_error(FREE_IMAGE_FORMAT fif, const char *message)
{
	if(fif != FIF_UNKNOWN)
	{
		printf("%s Format\n", FreeImage_GetFormatFromFIF(fif));
	}

	printf("%s\n", message);
}

static void TW_CALL set_callback(const void *value, void *client_data)
{
	int index = *static_cast<const int *>(value);
	ENV_MAPS map = static_cast<ENV_MAPS>(index);
	static_cast<Engine *>(client_data)->SetCurrentEnvMap(map);
	
}
static void TW_CALL get_callback(void *value, void *client_data)
{
	ENV_MAPS map = static_cast<Engine *>(client_data)->GetCurrentEnvMap();
	int index = static_cast<int>(map);
	*static_cast<int *>(value) = index;
}

/* ----------------------------------------------------------
 * Engine methods implementation
 * ----------------------------------------------------------
 */
Engine::Engine()
{
	InitGLFW();
	InitOpenGL();
	InitTweakBar();
	InitFreeImage();
	LoadData();
}

Engine::~Engine()
{
	TwTerminate();
	glfwDestroyWindow(window);
	FreeImage_DeInitialise();
	glfwTerminate();
}

void Engine::InitGLFW()
{
	glfwSetErrorCallback(error_callback);

	if (!glfwInit()) {
		exit(EXIT_FAILURE);
	}

	window = glfwCreateWindow(640, 480, "BRDF Toy", NULL, NULL);
	if (!window) {
		glfwTerminate();
		exit(EXIT_FAILURE);
	}

	glfwMakeContextCurrent(window);
	glfwGetFramebufferSize(window, &fb_width, &fb_height);
	glfwSwapInterval(1);

	glfwSetWindowUserPointer(window, this);

	glfwSetKeyCallback(window, key_callback);
	glfwSetCursorPosCallback(window, cursor_position_callback);
	glfwSetMouseButtonCallback(window, mouse_button_callback);
	glfwSetScrollCallback(window, scroll_callback);
	glfwSetCharCallback(window, character_callback);
	glfwSetWindowSizeCallback(window, window_size_callback);
	glfwSetFramebufferSizeCallback(window, framebuffer_size_callback);
	glfwSetWindowCloseCallback(window, close_callback);
}

void Engine::InitOpenGL()
{
	// Loading OpenGL functions
	gl::exts::LoadTest did_load = gl::sys::LoadFunctions();
	if(!did_load) {
		fprintf(stderr, "Error : OpenGl failed to load\n");
		exit(EXIT_FAILURE);
	}

	printf("Number of OpenGL functions that faild to load : %i\n", did_load.GetNumMissing());

	const GLubyte *renderer = gl::GetString(gl::RENDERER);
	const GLubyte *vendor = gl::GetString(gl::VENDOR);
	const GLubyte *version = gl::GetString(gl::VERSION);
	const GLubyte *glslVersion = gl::GetString(gl::SHADING_LANGUAGE_VERSION);

	GLint major, minor;
	gl::GetIntegerv(gl::MAJOR_VERSION, &major);
	gl::GetIntegerv(gl::MINOR_VERSION, &minor);

	printf("GL Vendor               : %s\n",    vendor);
	printf("GL Renderer             : %s\n",    renderer);
	printf("GL Version (string)     : %s\n",    version);
	printf("GL Version (integer)    : %i.%i\n", major, minor);
	printf("GLSL Version            : %s\n",    glslVersion);

}

void Engine::InitTweakBar()
{
	TwInit(TW_OPENGL_CORE, NULL);
	TwWindowSize(fb_width, fb_height);
	
	main_menu = TwNewBar("Menu");

	current_env_map = GRACE_POLAR;
	env_maps[0] = { GRACE_POLAR, "grace_polar.pfm" };
	env_maps[1] = { TEST_BOX, "test_box.pfm" };
	env_maps[2] = { GRAY_PLANE, "gray_plane.pfm" };

	current_brdf = LAMBERTIAN;
	brdfs[0] = { LAMBERTIAN, "lambert" };
	brdfs[1] = { PHONG, "phong" };

	TwType env_maps_type = TwDefineEnum("env_maps", env_maps, 3);
	TwAddVarCB(main_menu, "Env Map", env_maps_type, set_callback, get_callback, this, NULL);

	TwAddSeparator(main_menu, "", NULL);

	TwType brdfs_type = TwDefineEnum("brdfs", brdfs, 2);
	TwAddVarRW(main_menu, "BRDF", brdfs_type, &current_brdf, NULL);

	importance_sampling = false;
	shininess = 50;
	samples_number = 10;

	TwAddVarRW(main_menu, "Importance Smp", TW_TYPE_BOOL16, &importance_sampling, NULL);
	TwAddVarRW(main_menu, "Samples Num", TW_TYPE_UINT16, &samples_number, NULL);
	TwAddVarRW(main_menu, "Shininess", TW_TYPE_UINT16, &shininess, NULL);

	color_diffuse[0] = 1.0f;
	color_diffuse[1] = 1.0f;
	color_diffuse[2] = 1.0f;

	color_specular[0] = 1.0f;
	color_specular[1] = 1.0f;
	color_specular[2] = 1.0f;

	TwAddVarRW(main_menu, "Color Diffuse", TW_TYPE_COLOR3F, &color_diffuse, "colormode=rgb");
	TwAddVarRW(main_menu, "Color Specular", TW_TYPE_COLOR3F, &color_specular, "colormode=rgb");

	printf("%s\n", env_maps[current_env_map].Label);
}

void Engine::InitFreeImage()
{
	FreeImage_Initialise();

	const char * version = FreeImage_GetVersion();
	const char * copyright = FreeImage_GetCopyrightMessage();

	printf("FreeImage library version %s\n", version);
	printf("FreeImage Copyright : %s\n", copyright);

	FreeImage_SetOutputMessage(freeimage_error);
}

void Engine::LoadData()
{
	// Set up Viewport and shaders
	gl::Viewport(0, 0, fb_width, fb_height);

	camera_polar = 0.9f;
	camera_azimuth = 0.0f;

	camera_shader = gl::CreateProgram();
	AttachShader(camera_shader, gl::VERTEX_SHADER, "vert.glsl");
	AttachShader(camera_shader, gl::FRAGMENT_SHADER, "montecarlo.glsl");

	// Set up vertex array object and vertex buffer object
	gl::GenBuffers(1, &vbo_quad);
	gl::BindBuffer(gl::ARRAY_BUFFER, vbo_quad);
	gl::BufferData(gl::ARRAY_BUFFER, sizeof(quad_verts), quad_verts, gl::STATIC_DRAW);

	gl::GenVertexArrays(1, &vao_quad);
	gl::BindVertexArray(vao_quad);

	gl::EnableVertexAttribArray(0);
	gl::VertexAttribPointer(0, 2, gl::FLOAT, gl::FALSE_, 0, NULL);

	gl::BindVertexArray(NULL);

	// Set up environment map
	std::string file_name = std::string(env_maps[current_env_map].Label);
	file_name = "resources\\" + file_name;

	FIBITMAP *bitmap = FreeImage_Load(FIF_PFM, file_name.c_str());
	FreeImage_FlipVertical(bitmap);

	gl::GenTextures(1, &env_map);
	gl::BindTexture(gl::TEXTURE_2D, env_map);
	gl::TexParameteri(gl::TEXTURE_2D, gl::TEXTURE_WRAP_S, gl::REPEAT);
	gl::TexParameteri(gl::TEXTURE_2D, gl::TEXTURE_WRAP_T, gl::REPEAT);
	gl::TexParameteri(gl::TEXTURE_2D, gl::TEXTURE_MIN_FILTER, gl::LINEAR);
	gl::TexParameteri(gl::TEXTURE_2D, gl::TEXTURE_MAG_FILTER, gl::LINEAR);
	gl::TexImage2D(gl::TEXTURE_2D, 0, gl::RGB32F, FreeImage_GetWidth(bitmap), FreeImage_GetHeight(bitmap),
		0, gl::RGB, gl::FLOAT, FreeImage_GetBits(bitmap));

	// Set up uniform variables' locations
	resolution_unf = gl::GetUniformLocation(camera_shader, "resolution");
	look_at_angles_unf = gl::GetUniformLocation(camera_shader, "look_at_angles");
	brdf_unf = gl::GetUniformLocation(camera_shader, "brdf_number");
	importance_unf = gl::GetUniformLocation(camera_shader, "importance");
	samples_unf = gl::GetUniformLocation(camera_shader, "samples");
	shininess_unf = gl::GetUniformLocation(camera_shader, "shininess");
	color_diffuse_unf = gl::GetUniformLocation(camera_shader, "color_diffuse");
	color_specular_unf = gl::GetUniformLocation(camera_shader, "color_specular");
	env_map_unf = gl::GetUniformLocation(camera_shader, "env_map_texture");
}

void Engine::Run()
{
	// Main Rendering Cycle
	while (!glfwWindowShouldClose(window)) {
		
		time = glfwGetTime();

		gl::Clear(gl::COLOR_BUFFER_BIT);
		gl::ClearColor(0.0f, 0.0f, 0.0f, 1.0f);

		gl::UseProgram(camera_shader);

		gl::Uniform2f(resolution_unf, static_cast<float>(fb_width), static_cast<float>(fb_height));
		gl::Uniform2f(look_at_angles_unf, camera_azimuth, camera_polar);
		gl::Uniform1i(brdf_unf, brdfs[current_brdf].Value);
		gl::Uniform1i(importance_unf, static_cast<int>(importance_sampling));
		gl::Uniform1i(samples_unf, samples_number);
		gl::Uniform1f(shininess_unf, static_cast<float>(shininess));
		gl::Uniform3fv(color_diffuse_unf, 1, &color_diffuse[0]);
		gl::Uniform3fv(color_specular_unf, 1, &color_specular[0]);

		gl::BindTexture(gl::TEXTURE_2D, env_map);
		gl::Uniform1i(env_map_unf, 0);

		gl::BindVertexArray(vao_quad);
		gl::DrawArrays(gl::TRIANGLE_STRIP, 0, 4);
		gl::BindVertexArray(NULL);

		gl::BindTexture(gl::TEXTURE_2D, NULL);
		gl::UseProgram(NULL);

		TwDraw();

		glfwSwapBuffers(window);
		glfwPollEvents();
	}
}

void Engine::AttachShader(GLuint shader_program, GLenum type, const char * filename)
{
	/*
	 * Since OpenGL requests *const *string as a source code
	 * of a shader we need to place it within the const array
	 */
	const GLchar* code_array[1];
	GLuint shader_id = gl::CreateShader(type);

	std::string shader_code = ReadShaderCode(filename);
	code_array[0] = shader_code.c_str();

	gl::ShaderSource(shader_id, 1, code_array, 0);
	gl::CompileShader(shader_id);

	if(!CheckShaderStatus(shader_id, filename))
	{
		return;
	}

	gl::AttachShader(shader_program, shader_id);
	gl::LinkProgram(shader_program);

	if(!CheckProgramStatus(shader_program))
	{
		return;
	}
	
	gl::DeleteShader(shader_id);
}

std::string Engine::ReadShaderCode(const char *filename)
{
	std::ifstream file_input(filename);

	if(!file_input.good())
	{
		printf("Shader source code failed to load from %s\n", filename);
		return "";
	}

	return std::string(
		std::istreambuf_iterator<char>(file_input),
		std::istreambuf_iterator<char>());
}

bool Engine::CheckShaderStatus(GLuint shader_id, const char *shader_name)
{
	GLint compile_status;
	gl::GetShaderiv(shader_id, gl::COMPILE_STATUS, &compile_status);

	if(compile_status != gl::TRUE_)
	{
		GLint info_log_lenth;
		gl::GetShaderiv(shader_id, gl::INFO_LOG_LENGTH, &info_log_lenth);

		GLchar* buffer = new GLchar[info_log_lenth];
		GLsizei buffer_size;

		printf("%s : ", shader_name);

		gl::GetShaderInfoLog(shader_id, info_log_lenth, &buffer_size, buffer);
		printf(buffer);

		delete[] buffer;
		return false;
	}

	return true;
}

bool Engine::CheckProgramStatus(GLuint program_id)
{
	GLint link_status;
	gl::GetProgramiv(program_id, gl::LINK_STATUS, &link_status);

	if (link_status != gl::TRUE_)
	{
		GLint info_log_lenth;
		gl::GetProgramiv(program_id, gl::INFO_LOG_LENGTH, &info_log_lenth);

		GLchar* buffer = new GLchar[info_log_lenth];
		GLsizei bufferSize;

		gl::GetProgramInfoLog(program_id, info_log_lenth, &bufferSize, buffer);
		printf(buffer);

		delete[] buffer;
		return false;
	}

	return true;
}

void Engine::SetCurrentEnvMap(ENV_MAPS map)
{
	current_env_map = map;

	std::string file_name = std::string(env_maps[current_env_map].Label);
	file_name = "resources\\" + file_name;

	FIBITMAP *bitmap = FreeImage_Load(FIF_PFM, file_name.c_str());
	FreeImage_FlipVertical(bitmap);

	gl::BindTexture(gl::TEXTURE_2D, env_map);
	gl::TexParameteri(gl::TEXTURE_2D, gl::TEXTURE_WRAP_S, gl::REPEAT);
	gl::TexParameteri(gl::TEXTURE_2D, gl::TEXTURE_WRAP_T, gl::REPEAT);
	gl::TexParameteri(gl::TEXTURE_2D, gl::TEXTURE_MIN_FILTER, gl::LINEAR);
	gl::TexParameteri(gl::TEXTURE_2D, gl::TEXTURE_MAG_FILTER, gl::LINEAR);
	gl::TexImage2D(gl::TEXTURE_2D, 0, gl::RGB32F, FreeImage_GetWidth(bitmap), FreeImage_GetHeight(bitmap),
		0, gl::RGB, gl::FLOAT, FreeImage_GetBits(bitmap));
}

ENV_MAPS Engine::GetCurrentEnvMap()
{
	return current_env_map;
}

void Engine::SetCameraAzimuth(float azimuth)
{
	camera_azimuth = azimuth;
}

float Engine::GetCameraAzimuth()
{
	return camera_azimuth;
}

void Engine::SetCameraPolar(float polar)
{
	camera_polar = polar;
}

float Engine::GetCameraPolar()
{
	return camera_polar;
}

void Engine::SetFBWidth(int width)
{
	fb_width = width;
}

int Engine::GetFBWidth()
{
	return fb_width;
}

void Engine::SetFBHeight(int height)
{
	fb_height = height;
}

int Engine::GetFBHeight()
{
	return fb_height;
}
