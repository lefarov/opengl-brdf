#pragma once
#include "gl_core_4_5.hpp"
#include <glfw3.h>
#include <stdio.h>
#include <fstream>
#include <string>
#include <AntTweakBar.h>
#include <FreeImage.h>

/* ----------------------------------------------------------
* Vertex Data description
* -----------------------------------------------------------
*/
const static float quad_verts[] = {
	-1.0f, -1.0f,
	+1.0f, -1.0f,
	-1.0f, +1.0f,
	+1.0f, +1.0f
};

/* -----------------------------------------------------------
 * Enumerations used by AntTweakBar
 * -----------------------------------------------------------
 */
typedef enum { GRACE_POLAR, TEST_BOX, GRAY_PLANE } ENV_MAPS;
typedef enum { LAMBERTIAN, PHONG } BRDFS;

/* -----------------------------------------------------------
 * Engine Class
 * -----------------------------------------------------------
 */
class Engine
{
public:
	Engine();
	~Engine();

	void Run();

	void AttachShader(GLuint shader_program, GLenum type, const char* filename);
	std::string ReadShaderCode(const char* filename);
	bool CheckShaderStatus(GLuint shader_id, const char *shader_name);
	bool CheckProgramStatus(GLuint program_id);

	void SetCurrentEnvMap(ENV_MAPS map);
	ENV_MAPS GetCurrentEnvMap();

	void SetCameraAzimuth(float azimuth);
	float GetCameraAzimuth();

	void SetCameraPolar(float polar);
	float GetCameraPolar();

	void SetFBWidth(int width);
	int GetFBWidth();

	void SetFBHeight(int height);
	int GetFBHeight();

private:
	void InitGLFW();
	void InitOpenGL();
	void InitTweakBar();
	void InitFreeImage();
	void LoadData();

	GLFWwindow * window;
	TwBar * main_menu;

	GLuint camera_shader;
	GLuint vao_quad;
	GLuint vbo_quad;
	GLuint env_map;

	GLint resolution_unf;
	GLint look_at_angles_unf;
	GLint brdf_unf;
	GLint importance_unf;
	GLint samples_unf;
	GLint shininess_unf;
	GLint color_diffuse_unf;
	GLint color_specular_unf;
	GLint env_map_unf;

	ENV_MAPS current_env_map;
	TwEnumVal env_maps[3];

	BRDFS current_brdf;
	TwEnumVal brdfs[2];

	int shininess;
	int samples_number;
	bool importance_sampling;

	float color_diffuse[3];
	float color_specular[3];

	float camera_azimuth;
	float camera_polar;

	int fb_height;
	int fb_width;

	double time;
};

