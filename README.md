# BRDF Toy #

Simple program implements two most common BRDFs using Monte Carlo sampling, for presentational purpose. Rendering of the final image exploits Image-order rendering algoritm - calulates the radiance along the ray, goes through the *(x,y)* coordinate of image plane to observer position. All functionality, related to radiance and BRDF calculation is implemented in fragment shader **montecarlo.glsl**. Besides, simple rendering engine was implemented, responsible for OpenGL initialization, sending data to GPU, managing GLFW windows and Handling the GUI.

### Supported BRDF ###

* Lambertian
* Phong

### GUI Description ###

* **Env Map** - Select the Environment map
* **BRDF** - Select the BRDF
* **Importance Smp** - Choose between standard and importance Monte Carlo sampling
* **Samples Num** - Number of rays of integration for every point of the mesh
* **Shiness** - Exponent of Phong BRDF
* **Color Diffuse** - Diffuse component of mesh material
* **Color Specular** - Specular component of mesh material
* **WASD** - Camera movement

### Open Source Libraries ###

* GLFW
* AntTweakBar
* FreeImage
* GLM

### Screenshots ###
![brdf_toy1.png](https://bitbucket.org/repo/RdxxGb/images/2390664509-brdf_toy1.png)
![brdf_toy2.png](https://bitbucket.org/repo/RdxxGb/images/3988695803-brdf_toy2.png)
![brdf_toy3.png](https://bitbucket.org/repo/RdxxGb/images/3224461696-brdf_toy3.png)
![brdf_toy4.png](https://bitbucket.org/repo/RdxxGb/images/298111478-brdf_toy4.png)
![brdf_toy5.png](https://bitbucket.org/repo/RdxxGb/images/500786038-brdf_toy5.png)
![brdf_toy6.png](https://bitbucket.org/repo/RdxxGb/images/1376479280-brdf_toy6.png)
![brdf_toy7.png](https://bitbucket.org/repo/RdxxGb/images/3313033061-brdf_toy7.png)